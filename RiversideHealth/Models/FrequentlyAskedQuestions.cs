﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Identity;
using System.ComponentModel.DataAnnotations.Schema;
using System.ComponentModel.DataAnnotations;

namespace RiversideHealth.Models
{
    public class FrequentlyAskedQuestions
    {
        [Key]
        public int FaqID { get; set; }

        [Required, StringLength(int.MaxValue), Display(Name = "Question")]
        public string FaqQuestion { get; set; }

        [Required, StringLength(int.MaxValue), DataType(DataType.MultilineText), Display(Name = "Answer")]
        public string FaqAnswer { get; set; }

        //false for inactive, true for active.
        [Required, Display(Name = "Status")]
        public bool IsPublished { get; set; }

        [Required, DataType(DataType.DateTime), Display(Name = "Created On")]
        public DateTime DateCreated { get; set; }

        [ForeignKey("AdminId")]
        public int AdminId { get; set; }

        public virtual Admin Admin { get; set; }
    }
}
