﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Identity;
using System.ComponentModel.DataAnnotations.Schema;
using System.ComponentModel.DataAnnotations;


namespace RiversideHealth.Models
{
    public class Career
    {
        [Key]
        public int CareerId { get; set; }

        [Required, DataType(DataType.DateTime), Display(Name = "Date")]
        public DateTime Date { get; set; }

        [Required, StringLength(100), Display(Name = "Career Name")]
        public string CareerName { get; set; }

        //0 for volunteer role, 1 for paid role
        public int CareerType { get; set; }

        [Required, StringLength(int.MaxValue), Display(Name = "Career Description")]
        public string CareerDescription { get; set; }

        //0 for inactive, 1 for active. If position is filled, don't display on careers page.
        [Required, Display(Name = "Status")]
        public int Status { get; set; }

        [Required, Display(Name = "Wage")]
        public float Wage { get; set; }

        [Required, StringLength(100), Display(Name = "Contact")]
        public string ContactName { get; set; }

        [Required, StringLength(225), Display(Name = "Contact Email")]
        public string ContactEmail { get; set; }

        [ForeignKey("AdminId")]
        public int AdminId { get; set; }

        public virtual Admin Admin { get; set; }

    }
}
