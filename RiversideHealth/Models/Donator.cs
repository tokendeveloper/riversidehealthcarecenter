﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using Microsoft.AspNetCore.Identity;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;


namespace RiversideHealth.Models
{
    public class Donator
    {
        [Key]
        public int DonatorId { get; set; }

        [Required, StringLength(50), Display(Name = "Reference ID")]
        public string DonatorRefID { get; set; }

        [Required, StringLength(50), Display(Name = "First Name")]
        public string DonatorFirstName { get; set; }

        [Required, StringLength(50), Display(Name = "Last Name")]
        public string DonatorLastName { get; set; }

        [Required, StringLength(50), Display(Name = "Email")]
        public string DonatorEmail { get; set; }

        [Required, StringLength(50), Display(Name = "Phone")]
        public string DonatorPhone { get; set; }

        //0 for True, 1 for False.

        // set default values
        // reference: https://stackoverflow.com/questions/23823103/default-value-in-mvc-model-using-data-annotation
        // author: Nilesh, Jeppe Stig Nelsen

        [Display(Name = "I want to be Anonymous")]
        public int IsAnonymous { get; set; } = 0;

        // one donator can have many transactions
        [InverseProperty("Donator")]
        public virtual List<Transaction> Transactions { get; set; }
    }
}
