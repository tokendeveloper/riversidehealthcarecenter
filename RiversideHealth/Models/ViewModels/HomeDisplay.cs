﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace RiversideHealth.Models.ViewModels
{
    public class HomeDisplay
    {
        public HomeDisplay()
        {

        }
        public IEnumerable<Page> Pages { get; set; }
        public IEnumerable<Announcement> Announcements { get; set; } 

        public IEnumerable<Admin> Admins { get; set; }
    }

}
