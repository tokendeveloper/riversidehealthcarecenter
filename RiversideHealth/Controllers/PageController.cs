﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using System.Data.SqlClient;
using RiversideHealth.Models;
using RiversideHealth.Models.ViewModels;
using RiversideHealth.Data;
using Microsoft.AspNetCore.Http;

//References Christine's blog controller.

namespace RiversideHealth.Controllers
{
    public class PageController : Controller
    {
        private readonly RiversideHealthDbContext db;
        private readonly UserManager<ApplicationUser> _userManager;
        private async Task<ApplicationUser> GetCurrentUserAsync() => await _userManager.GetUserAsync(HttpContext.User);

        public PageController(RiversideHealthDbContext context, UserManager<ApplicationUser> usermanager)
        {
            db = context;
            _userManager = usermanager;
        }
        public async Task<ActionResult> Index(int pagenum)
        {
            var user = await GetCurrentUserAsync();
            if (user != null)
            {
                if (user.AdminId == null) { ViewData["UserHasAdmin"] = "False"; }
                else { ViewData["UserHasAdmin"] = user.AdminId.ToString(); }
            }
            else
            {
                ViewData["UserHasAdmin"] = "None";
            }
            //taken from christine's code
            /*Pagination Algorithm*/

            var _pages = await db.Pages.Include(a => a.Admin).ToListAsync();
            int pagecount = _pages.Count();
            int perpage = 3;
            int maxpage = (int)Math.Ceiling((decimal)pagecount / perpage) - 1;
            if (maxpage < 0) maxpage = 0;
            if (pagenum < 0) pagenum = 0;
            if (pagenum > maxpage) pagenum = maxpage;
            int start = perpage * pagenum;
            ViewData["pagenum"] = (int)pagenum;
            ViewData["PaginationSummary"] = "";
            if (maxpage > 0)
            {
                ViewData["PaginationSummary"] =
                    (pagenum + 1).ToString() + " of " +
                    (maxpage + 1).ToString();
            }
            List<Page> pages = await db.Pages.Include(a => a.Admin).Skip(start).Take(perpage).ToListAsync();
            /*End of Pagination  Algorithm*/

            return View(pages);
        }

        public async Task<ActionResult> PublicIndex(int pagenum)
        {
            var user = await GetCurrentUserAsync();
            if (user != null)
            {
                if (user.AdminId == null) { ViewData["UserHasAdmin"] = "False"; }
                else { ViewData["UserHasAdmin"] = user.AdminId.ToString(); }
            }
            else
            {
                ViewData["UserHasAdmin"] = "None";
            }
            //taken from christine's code
            /*Pagination Algorithm*/

            var _pages = await db.Pages.Include(a => a.Admin).ToListAsync();
            int pagecount = _pages.Count();

            for (int i = 0; i < _pages.Count(); i++)
            {
                if (_pages[i].PublishStatus == 0)
                {
                    _pages.Remove(_pages[i]);
                }
            }
            int perpage = 5;
            int maxpage = (int)Math.Ceiling((decimal)pagecount / perpage) - 1;
            if (maxpage < 0) maxpage = 0;
            if (pagenum < 0) pagenum = 0;
            if (pagenum > maxpage) pagenum = maxpage;
            int start = perpage * pagenum;
            ViewData["pagenum"] = (int)pagenum;
            ViewData["PaginationSummary"] = "";
            if (maxpage > 0)
            {
                ViewData["PaginationSummary"] =
                    (pagenum + 1).ToString() + " of " +
                    (maxpage + 1).ToString();
            }

            /*End of Pagination  Algorithm*/

            return View(_pages.Skip(start).Take(perpage));
        }

        public async Task<ActionResult> Create()
        {
            var user = await GetCurrentUserAsync();
            if (user != null)
            {
                if (user.AdminId == null) { ViewData["UserHasAdmin"] = "False"; }
                else { ViewData["UserHasAdmin"] = user.AdminId.ToString(); }
            }
            else
            {
                ViewData["UserHasAdmin"] = "None";
            }

            PageEdit pageEditView = new PageEdit();
            pageEditView.Admins = db.Admins.ToList();
            return View(pageEditView);
        }

        [HttpPost]
        public ActionResult Create(string PageTitle_New, string PageContent_New, int PageAuthor_New, int PublishStatus)
        {

            string query = "insert into Pages (Date, Title, Content, PublishStatus, AdminId) values (@date, @title, @content, @publishStatus, @admin)";
            SqlParameter[] myparams = new SqlParameter[5];
            myparams[0] = new SqlParameter("@date", DateTime.UtcNow);
            myparams[1] = new SqlParameter("@title", PageTitle_New);
            myparams[2] = new SqlParameter("@content", PageContent_New);
            myparams[3] = new SqlParameter("@publishStatus", PublishStatus);
            myparams[4] = new SqlParameter("@admin", PageAuthor_New);

            db.Database.ExecuteSqlCommand(query, myparams);

            return RedirectToAction("Index");
        }

        public async Task<ActionResult> Details(int id)
        {
            var user = await GetCurrentUserAsync();
            if (user != null)
            {
                if (user.AdminId == null) { ViewData["UserHasAdmin"] = "False"; }
                else { ViewData["UserHasAdmin"] = user.AdminId.ToString(); }
            }
            else
            {
                ViewData["UserHasAdmin"] = "None";
            }
            return View(db.Pages.Include(a => a.Admin).SingleOrDefault(p => p.PageId == id));
        }

        public ActionResult Delete(int id)
        {
            string query = "delete from pages where pageid = @id";
            db.Database.ExecuteSqlCommand(query, new SqlParameter("@id", id));

            return RedirectToAction("Index");
        }

        public async Task<ActionResult> Edit(int? id)
        {
            var user = await GetCurrentUserAsync();
            if (user != null)
            {
                if (user.AdminId == null) { ViewData["UserHasAdmin"] = "False"; }
                else { ViewData["UserHasAdmin"] = user.AdminId.ToString(); }
            }
            else
            {
                ViewData["UserHasAdmin"] = "None";
            }
            PageEdit pageEditView = new PageEdit();

            pageEditView.Page = db.Pages
                .Include(p => p.Admin)
                .SingleOrDefault(p => p.PageId == id);

            pageEditView.Admins = db.Admins.ToList();
            return View(pageEditView);
        }

        [HttpPost]
        public async Task<ActionResult> Edit(string Title, string Content, int PublishStatusEdit, int id)
        {
            var user = await GetCurrentUserAsync();
            if (user != null)
            {
                if (user.AdminId == null) { ViewData["UserHasAdmin"] = "False"; }
                else { ViewData["UserHasAdmin"] = user.AdminId.ToString(); }
            }
            else
            {
                ViewData["UserHasAdmin"] = "None";
            }

            if (db.Pages.Find(id) == null)
            {
                //Show error message
                return NotFound();

            }
            //Raw query data
            string query = "update pages set Title = @title, Content = @content, PublishStatus = @status WHERE pageId = @id";

            SqlParameter[] sqlParameters = new SqlParameter[4];
            sqlParameters[0] = new SqlParameter("@title", Title);
            sqlParameters[1] = new SqlParameter("@content", Content);
            sqlParameters[2] = new SqlParameter("@status", PublishStatusEdit);
            sqlParameters[3] = new SqlParameter("@id", id);


            db.Database.ExecuteSqlCommand(query, sqlParameters);


            return RedirectToAction("Index");
        }


    }
}