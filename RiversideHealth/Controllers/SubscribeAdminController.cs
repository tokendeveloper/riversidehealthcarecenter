﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Net.Mail;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using RiversideHealth.Models;

namespace RiversideHealth.Controllers
{
    public class SubscribeAdminController : Controller
    {
        string connString = Global.connString; // @"Data Source = (localdb)\mssqllocaldb; Initial Catalog = RiversideHealth; Integrated Security=True";
        // GET: SubscribeAdmin
        public ActionResult Index()
        {
            DataTable dbblContact = new DataTable();
            using (SqlConnection sqlcon = new SqlConnection(connString))
            {
                sqlcon.Open();
                SqlDataAdapter sqlda = new SqlDataAdapter("SELECT * FROM EmailUpdateForm", sqlcon);
                sqlda.Fill(dbblContact);
            }
            return View(dbblContact);
        }

        // GET: SubscribeAdmin/Details/5
        public ActionResult Details(int id)
        {
            return View();
        }

        // GET: SubscribeAdmin/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: SubscribeAdmin/Create
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(AddUpdateModel addUpdateModel)
        {
            using (SqlConnection sqlcon = new SqlConnection(connString))
            {
                sqlcon.Open();
                string quary = "INSERT INTO AddUpdate VALUES(@Email,@Title,@Des,@Date)";
                SqlCommand sqlcmd = new SqlCommand(quary, sqlcon);
                sqlcmd.Parameters.AddWithValue("@Email", addUpdateModel.AdminEmail);
                sqlcmd.Parameters.AddWithValue("@Title", addUpdateModel.Title);
                sqlcmd.Parameters.AddWithValue("@Des", addUpdateModel.Description);
                DateTime dateTime = DateTime.Now;
                sqlcmd.Parameters.AddWithValue("@Date", dateTime);                
                sqlcmd.ExecuteNonQuery();
            }
            DataTable datatable = new DataTable();
            using (SqlConnection sqlcon = new SqlConnection(connString))
            {
                sqlcon.Open();
                SqlDataAdapter sqlda = new SqlDataAdapter("SELECT VEmail FROM EmailUpdateForm", sqlcon);
                sqlda.Fill(datatable);
            }
            for (int i=0; i< datatable.Rows.Count;i++)
            {
                MailMessage mail = new MailMessage();
                mail.To.Add(datatable.Rows[i][0].ToString());
                mail.From = new MailAddress("pankajparikh199704@gmail.com");
                mail.Subject = addUpdateModel.Title;
                string Body = addUpdateModel.Description;
                mail.Body = Body;
                mail.IsBodyHtml = true;
                SmtpClient smtp = new SmtpClient();
                smtp.Host = "smtp.gmail.com";
                smtp.Port = 587;
                smtp.UseDefaultCredentials = false;
                smtp.Credentials = new System.Net.NetworkCredential("pankajparikh199704@gmail.com", "pankaj@123"); // Enter seders User name and password   
                smtp.EnableSsl = true;
                smtp.Send(mail);
            }
            return RedirectToAction("Create");
        }

        // GET: SubscribeAdmin/Edit/5
        public ActionResult Edit(int id)
        {
            return View();
        }

        // POST: SubscribeAdmin/Edit/5
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(int id, IFormCollection collection)
        {
            try
            {
                // TODO: Add update logic here

                return RedirectToAction(nameof(Index));
            }
            catch
            {
                return View();
            }
        }

        // GET: SubscribeAdmin/Delete/5
        public ActionResult Delete(int id)
        {
            return View();
        }

        // POST: SubscribeAdmin/Delete/5
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Delete(int id, IFormCollection collection)
        {
            try
            {
                // TODO: Add delete logic here

                return RedirectToAction(nameof(Index));
            }
            catch
            {
                return View();
            }
        }
    }
}