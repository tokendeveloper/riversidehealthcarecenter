﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using System.Data.SqlClient;
using RiversideHealth.Models;
using RiversideHealth.Models.ViewModels;
using RiversideHealth.Data;
using Microsoft.AspNetCore.Http;

namespace RiversideHealth.Controllers
{
    public class FrequentlyAskedQuestionController : Controller
    {

        private readonly RiversideHealthDbContext db;
        private readonly UserManager<ApplicationUser> _userManager;
        private async Task<ApplicationUser> GetCurrentUserAsync() => await _userManager.GetUserAsync(HttpContext.User);

        public FrequentlyAskedQuestionController(RiversideHealthDbContext context, UserManager<ApplicationUser> usermanager)
        {
            db = context;
            _userManager = usermanager;
        }

        public IActionResult Index()
        {
            return View();
        }

        //User

        public async Task<ActionResult> FaqUserList(int pagenum)
        {
            var user = await GetCurrentUserAsync();

            if (user != null)
            {
                if (user.AdminId != null)
                {
                    return RedirectToAction("FaqAdminList");
                }
            }

            /*Pagination Algorithm*/
            var _faq = await db.FrequentlyAskedQuestions.Where(a=>a.IsPublished==true).ToListAsync();
            int pagecount = _faq.Count();
            int perpage = 3;
            int maxpage = (int)Math.Ceiling((decimal)pagecount / perpage) - 1;
            if (maxpage < 0) maxpage = 0;
            if (pagenum < 0) pagenum = 0;
            if (pagenum > maxpage) pagenum = maxpage;
            int start = perpage * pagenum;
            ViewData["pagenum"] = (int)pagenum;
            ViewData["PaginationSummary"] = "";
            if (maxpage > 0)
            {
                ViewData["PaginationSummary"] =
                    (pagenum + 1).ToString() + " of " +
                    (maxpage + 1).ToString();
            }
            List<FrequentlyAskedQuestions> frequentlyAskedQuestions = await db.FrequentlyAskedQuestions.Where(a => a.IsPublished == true).Skip(start).Take(perpage).ToListAsync();
            /*End of Pagination  Algorithm*/

            return View(frequentlyAskedQuestions);

        }

        public ActionResult FaqUserDetail(int id)
        {
            //List<FrequentlyAskedQuestions> frequentlyAskedQuestions = await db.FrequentlyAskedQuestions.Where(a => a.FaqID ==id).ToListAsync();

            //return View(frequentlyAskedQuestions);

            return View(db.FrequentlyAskedQuestions.SingleOrDefault(p => p.FaqID == id));
        }
        
        //Admin  
        public async Task<ActionResult> FaqAdminList(int pagenum)
        {
            var user = await GetCurrentUserAsync();
        
            if (user != null)
            {
                if (user.AdminId == null) { ViewData["UserIsAdmin"] = "false"; }
                else { ViewData["UserIsAdmin"] = user.AdminId.ToString(); }
            }
            else
            {
                ViewData["UserIsAdmin"] = "false";
            }

            /*Pagination Algorithm*/
            var _faq = await db.FrequentlyAskedQuestions.Include(a => a.Admin).ToListAsync();
            int pagecount = _faq.Count();
            int perpage = 10;
            int maxpage = (int)Math.Ceiling((decimal)pagecount / perpage) - 1;
            if (maxpage < 0) maxpage = 0;
            if (pagenum < 0) pagenum = 0;
            if (pagenum > maxpage) pagenum = maxpage;
            int start = perpage * pagenum;
            ViewData["pagenum"] = (int)pagenum;
            ViewData["PaginationSummary"] = "";
            if (maxpage > 0)
            {
                ViewData["PaginationSummary"] =
                    (pagenum + 1).ToString() + " of " +
                    (maxpage + 1).ToString();
            }
            List<FrequentlyAskedQuestions> faq = await db.FrequentlyAskedQuestions.Include(a => a.Admin).Skip(start).Take(perpage).ToListAsync();
            /*End of Pagination  Algorithm*/

            return View(faq);
        }

        public async Task<ActionResult> FaqAdminCreate()
        {
            var user = await GetCurrentUserAsync();
            if (user != null)
            {
                if (user.AdminId == null) { ViewData["UserIsAdmin"] = "false"; }
                else { ViewData["UserIsAdmin"] = user.AdminId.ToString(); }
            }
            else
            {
                ViewData["UserIsAdmin"] = "false";
            }            
            return View();
        }

        [HttpPost]
        public async Task<ActionResult> FaqAdminCreate(string FaqQuestion, string FaqAnswer, bool IsPublished)
        {
            var user = await GetCurrentUserAsync();
            if (user != null)
            {
                if (user.AdminId != null) {

                    string query = "insert into dbo.Faq (FaqQuestion, FaqAnswer, IsPublished, DateCreated, AdminId) values (@FaqQuestion, @FaqAnswer, @IsPublished, @DateCreated, @AdminId)";
                    SqlParameter[] myparams = new SqlParameter[9];
                    myparams[0] = new SqlParameter("@FaqQuestion", FaqQuestion);
                    myparams[1] = new SqlParameter("@FaqAnswer", FaqAnswer);
                    myparams[2] = new SqlParameter("@IsPublished", IsPublished);
                    myparams[3] = new SqlParameter("@DateCreated", DateTime.UtcNow);
                    myparams[5] = new SqlParameter("@AdminId", user.AdminId.ToString());

                    db.Database.ExecuteSqlCommand(query, myparams);
                    return RedirectToAction("FaqAdminList");
                }

            }        

            return RedirectToAction("FaqAdminList");
        }


        public async Task<ActionResult> FaqAdminEdit(int id)
        {
            //List<FrequentlyAskedQuestions> frequentlyAskedQuestions = await db.FrequentlyAskedQuestions.Where(a => a.FaqID ==id).ToListAsync();

            //return View(frequentlyAskedQuestions);


            //get current user details
            var user = await GetCurrentUserAsync();
            if (user != null)
            {
                if (user.AdminId == null) { ViewData["UserIsAdmin"] = "false"; }
                else { ViewData["UserIsAdmin"] = user.AdminId.ToString(); }
                return View(db.FrequentlyAskedQuestions.SingleOrDefault(p => p.FaqID == id));
            }
            else
            {
                ViewData["UserIsAdmin"] = "false";
            }
            return RedirectToAction("FaqAdminList");
        }

        [HttpPost]
        public async Task<ActionResult> FaqAdminEdit(int FaqID, string FaqQuestion, string FaqAnswer, bool IsPublished)
        {
            var user = await GetCurrentUserAsync();
            if (user != null)
            {
                if (user.AdminId != null)
                {

                    string query = "UPDATE dbo.Faq SET FaqQuestion=@FaqQuestion, FaqAnswer=@FaqAnswer, IsPublished=@IsPublished,DateCreated = @DateCreated, AdminId=@AdminId WHERE FaqID=@FaqID";
                    SqlParameter[] myparams = new SqlParameter[9];
                    myparams[0] = new SqlParameter("@FaqQuestion", FaqQuestion);
                    myparams[1] = new SqlParameter("@FaqAnswer", FaqAnswer);
                    myparams[2] = new SqlParameter("@IsPublished", IsPublished);
                    myparams[3] = new SqlParameter("@DateCreated", DateTime.UtcNow);
                    myparams[4] = new SqlParameter("@AdminId", user.AdminId);
                    myparams[5] = new SqlParameter("@FaqID", FaqID);

                    db.Database.ExecuteSqlCommand(query, myparams);
                    return RedirectToAction("FaqAdminList");
                }

            }

            return RedirectToAction("FaqAdminList");
        }



    }
}