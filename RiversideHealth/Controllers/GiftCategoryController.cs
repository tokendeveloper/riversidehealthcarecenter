﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using System.Data.SqlClient;
using RiversideHealth.Models;
using RiversideHealth.Data;
using Microsoft.AspNetCore.Http;
using System.Diagnostics;

namespace RiversideHealth.Controllers
{
    public class GiftCategoryController : Controller
    {
        private readonly RiversideHealthDbContext context;
        private readonly UserManager<ApplicationUser> _userManager;
        private async Task<ApplicationUser> GetCurrentUserAsync() => await _userManager.GetUserAsync(HttpContext.User);

        public GiftCategoryController(RiversideHealthDbContext ctx, UserManager<ApplicationUser> userManager)
        {
            context = ctx;
            _userManager = userManager;
        }

        public async Task<ActionResult> Index(int page)
        {
            // automatically create an uncategorized category
            // this is for deletion purposes

            var admin = await GetCurrentUserAsync();
            if (admin != null)
            {
                if (admin.AdminId == null)
                {
                    ViewData["UserIsAdmin"] = "False";
                    // redirect if user isnt logged in
                    return Redirect("/Account/Login");
                }
                else
                {
                    ViewData["UserIsAdmin"] = admin.AdminId.ToString();
                }
            }
            else
            {
                ViewData["UserIsAdmin"] = "False";
                // redirect if user isnt logged in
                return Redirect("/Account/Login");
            }

            if (context.GiftCategories.Any(c => c.GiftCategoryName.Equals("Uncategorized") == true))
            {
                Debug.WriteLine("It Exists");
            }
            else
            {
                GiftCategory uncategorized = new GiftCategory()
                {
                    GiftCategoryName = "Uncategorized"
                };

                // insert query
                context.GiftCategories.Add(uncategorized);
                context.SaveChanges();
            }

            /*Pagination Algorithm*/
            // I based this on christine's code
            var myCategories = await context.GiftCategories.ToListAsync();
            int pagecount = myCategories.Count();
            int pageItems = 8;
            int limit = (int)Math.Ceiling((decimal)pagecount / pageItems) - 1;

            limit = limit < 0 ? 0 : limit;
            page = page < 0 ? 0 : page;
            page = page > limit ? limit : page;

            int initial = pageItems * page;

            ViewData["page"] = (int)page;
            ViewData["PageSummary"] = "";

            if (limit > 0)
            {
                ViewData["PageSummary"] =
                    (page + 1).ToString() + " of " +
                    (limit + 1).ToString();
            }

            List<GiftCategory> categories = await context.GiftCategories.Skip(initial).Take(pageItems).ToListAsync();

            return View(categories);
        }

        public ActionResult Details(int id)
        {
            return View(context.GiftCategories.Find(id));
        }

        public async Task<ActionResult> Create()
        {
            var admin = await GetCurrentUserAsync();
            if (admin != null)
            {
                if (admin.AdminId == null)
                {
                    ViewData["UserIsAdmin"] = "False";
                    // redirect if user isnt logged in
                    return Redirect("/Account/Login");
                }
                else
                {
                    ViewData["UserIsAdmin"] = admin.AdminId.ToString();
                }
            }
            else
            {
                ViewData["UserIsAdmin"] = "False";
                // redirect if user isnt logged in
                return Redirect("/Account/Login");
            }

            return View();
        }

        public async Task<ActionResult> Edit(int id)
        {
            var admin = await GetCurrentUserAsync();
            if (admin != null)
            {
                if (admin.AdminId == null)
                {
                    ViewData["UserIsAdmin"] = "False";
                    // redirect if user isnt logged in
                    return Redirect("/Account/Login");
                }
                else
                {
                    ViewData["UserIsAdmin"] = admin.AdminId.ToString();
                }
            }
            else
            {
                ViewData["UserIsAdmin"] = "False";
                // redirect if user isnt logged in
                return Redirect("/Account/Login");
            }

            return View(context.GiftCategories.Find(id));
        }


        [HttpPost]
        public ActionResult Create([Bind("GiftCategoryName")] GiftCategory gc)
        {
            //insert query
            context.GiftCategories.Add(gc);

            //save
            context.SaveChanges();
            return RedirectToAction("Index");
        }

        [HttpPost]
        public ActionResult Edit(int id, string GiftCategoryName_Update)
        {
            //create new object
            // reference https://davidsekar.com/c-sharp/modify-or-delete-entity-object-without-select
            GiftCategory gc = new GiftCategory() { GiftCategoryId = id };
            
            // attach to entity
            context.GiftCategories.Attach(gc);

            // add the update
            gc.GiftCategoryName = GiftCategoryName_Update;

            // save the changes
            context.SaveChanges();

           return RedirectToAction("Details/" + id);
        }

        // implement async to be more secure

        public async Task<ActionResult> Delete(int id)
        {
            //create new object
            // reference https://davidsekar.com/c-sharp/modify-or-delete-entity-object-without-select

            var admin = await GetCurrentUserAsync();
            if (admin != null)
            {
                if (admin.AdminId == null)
                {
                    ViewData["UserIsAdmin"] = "False";
                    // redirect if user isnt logged in
                    return Redirect("/Account/Login");
                }
                else
                {
                    ViewData["UserIsAdmin"] = admin.AdminId.ToString();
                }
            }
            else
            {
                ViewData["UserIsAdmin"] = "False";
                // redirect if user isnt logged in
                return Redirect("/Account/Login");
            }

            // create an uncategorized category if it does not exist
            int catId = 0;

            if (context.GiftCategories.Any(c => c.GiftCategoryName == "Uncategorized"))
            {
                Debug.WriteLine("it exists");
                
                GiftCategory uncat = context.GiftCategories.Where(ct => ct.GiftCategoryName == "Uncategorized").FirstOrDefault();
                catId = uncat.GiftCategoryId;

                Debug.WriteLine("new ID: " + catId);

                // reset all dependent entries
                // we can set the category  to uncategorized
                string query = "UPDATE GiftItems SET GiftCategoryId = @uncat WHERE GiftCategoryId = @id";
                SqlParameter[] catSettings = new SqlParameter[2];

                catSettings[0] = new SqlParameter("@uncat", catId);
                catSettings[1] = new SqlParameter("@id", id);

                context.Database.ExecuteSqlCommand(query, catSettings);
            }

            // we can now delete the category we want because the dependents are transferred over uncategorized

            GiftCategory gc = new GiftCategory() { GiftCategoryId = id };

            // attach to entity
            context.GiftCategories.Attach(gc);

            // remove the record
            context.GiftCategories.Remove(gc);                                                                              

            // save the changes
            context.SaveChanges();

            return RedirectToAction("Index");
        }
    }
}