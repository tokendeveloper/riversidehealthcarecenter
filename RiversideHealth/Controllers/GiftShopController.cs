﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using System.Data.SqlClient;
using RiversideHealth.Models;
using RiversideHealth.Models.ViewModels;
using RiversideHealth.Data;
using Microsoft.AspNetCore.Http;
using System.Diagnostics;
using System.IO;
using Microsoft.AspNetCore.Hosting;

namespace RiversideHealth.Controllers
{
    public class GiftShopController : Controller
    {
        private readonly RiversideHealthDbContext context;
        private readonly IHostingEnvironment _dir;
        private readonly UserManager<ApplicationUser> _userManager;
        private async Task<ApplicationUser> GetCurrentUserAsync() => await _userManager.GetUserAsync(HttpContext.User);

        public GiftShopController(RiversideHealthDbContext ctx, IHostingEnvironment dir, UserManager<ApplicationUser> userManager)
        {
            context = ctx;
            _dir = dir;
            _userManager = userManager;
        }

        public async Task<ActionResult> Index(int page)
        {
            var admin = await GetCurrentUserAsync();
            if (admin != null)
            {
                if (admin.AdminId == null)
                {
                    ViewData["UserIsAdmin"] = "False";
                }
                else
                {
                    ViewData["UserIsAdmin"] = admin.AdminId.ToString();
                }
            }
            else
            {
                ViewData["UserIsAdmin"] = "False";
            }

            /*Pagination Algorithm*/

            var myCategories = await context.GiftItems.ToListAsync();
            int pagecount = myCategories.Count();
            int pageItems = 8;
            int limit = (int)Math.Ceiling((decimal)pagecount / pageItems) - 1;

            limit = limit < 0 ? 0 : limit;
            page = page < 0 ? 0 : page;
            page = page > limit ? limit : page;

            int initial = pageItems * page;

            ViewData["page"] = (int)page;
            ViewData["PageSummary"] = "";

            if (limit > 0)
            {
                ViewData["PageSummary"] =
                    (page + 1).ToString() + " of " +
                    (limit + 1).ToString();
            }

            List<GiftItems> ge = await context.GiftItems.Skip(initial).Take(pageItems).ToListAsync();

            //return the view
            return View(ge);
        }

        public ActionResult Details(int id)
        {
            GiftItemEdit ge = new GiftItemEdit();
            ge.GiftItems = context.GiftItems.Find(id);
            ge.GiftCategories = context.GiftCategories.Where(gc => gc.GiftCategoryId == ge.GiftItems.GiftCategoryId).ToList();
            return View(ge);
        }

        public async Task<ActionResult> Create()
        {
            var admin = await GetCurrentUserAsync();
            if (admin != null)
            {
                if (admin.AdminId == null)
                {
                    ViewData["UserIsAdmin"] = "False";
                    // redirect if user isnt logged in
                    return Redirect("/Account/Login");
                }
                else
                {
                    ViewData["UserIsAdmin"] = admin.AdminId.ToString();
                }
            }
            else
            {
                ViewData["UserIsAdmin"] = "False";
                // redirect if user isnt logged in
                return Redirect("/Account/Login");
            }

            // create gift edit entity
            GiftItemEdit ge = new GiftItemEdit();
            ge.GiftCategories = context.GiftCategories.ToList();
            ge.Locations = context.Locations.ToList();

            // pass the model to the view
            return View(ge);
        }

        public async Task<ActionResult> Edit(int id)
        {
            var admin = await GetCurrentUserAsync();
            if (admin != null)
            {
                if (admin.AdminId == null)
                {
                    ViewData["UserIsAdmin"] = "False";
                    // redirect if user isnt logged in
                    return Redirect("/Account/Login");
                }
                else
                {
                    ViewData["UserIsAdmin"] = admin.AdminId.ToString();
                }
            }
            else
            {
                ViewData["UserIsAdmin"] = "False";
                // redirect if user isnt logged in
                return Redirect("/Account/Login");
            }

            // create gift edit entity
            GiftItemEdit ge = new GiftItemEdit();
            ge.GiftCategories = context.GiftCategories.ToList();
            ge.GiftItems = context.GiftItems.Find(id);
            ge.Locations = context.Locations.ToList();

            return View(ge);
        }

        [HttpPost]
        public ActionResult Create(string GiftItemName_New, string GiftItemDescription_New, decimal GiftItemPrice_New, int GiftCategoryId_New)
        {

            GiftItems item = new GiftItems()
            {
                GiftItemName = GiftItemName_New,
                GiftItemDescription = GiftItemDescription_New,
                GiftItemPrice = GiftItemPrice_New,
                GiftCategoryId = GiftCategoryId_New
            };

            //Debug.WriteLine("Price: " + item.GiftItemPrice);
            //insert query
            context.GiftItems.Add(item);

            //save
            context.SaveChanges();
            return RedirectToAction("Index");
        }

        [HttpPost]
        public ActionResult Update(int id, string GiftItemName_Update, string GiftItemDescription_Update, decimal GiftItemPrice_Update, int GiftCategoryId_Update, IFormFile ItemImage)
        {
            //create new object
            // reference https://davidsekar.com/c-sharp/modify-or-delete-entity-object-without-select
            GiftItems item = new GiftItems()
            {
                GiftItemsId = id
            };
            
            // attach to entity
            context.GiftItems.Attach(item);

            // add the update
            item.GiftItemName = GiftItemName_Update;
            item.GiftItemDescription = GiftItemDescription_Update;
            item.GiftItemPrice = GiftItemPrice_Update;
            item.GiftCategoryId = GiftCategoryId_Update;

            var docFolder = _dir.WebRootPath;

            // check if a file awas uploaded
            if (ItemImage != null)
            {
                if (ItemImage.Length > 0)
                {
                    //file extensioncheck taken from https://www.c-sharpcorner.com/article/file-upload-extension-validation-in-asp-net-mvc-and-javascript/
                    var imgtypes = new[] { "jpeg", "jpg", "png", "gif" };
                    var extension = Path.GetExtension(ItemImage.FileName).Substring(1);

                    if (imgtypes.Contains(extension))
                    {
                        //generic .img extension, web translates easily.
                        string image = item.GiftItemsId + "." + extension;

                        //get a direct file path to imgs/authors/
                        string path = Path.Combine(docFolder, "images/gift-items");
                        path = Path.Combine(path, image);

                        //save the file
                        using (var stream = new FileStream(path, FileMode.Create))
                        {
                            ItemImage.CopyTo(stream);
                        }

                        item.HasImages = 1;
                        item.GiftItemImage = image;
                    }
                }
            }


            // save the changes
            context.SaveChanges();

            return Redirect("/GiftShop/Details/" + id);
        }

        public async Task<ActionResult> Delete(int id)
        {
            var admin = await GetCurrentUserAsync();
            if (admin != null)
            {
                if (admin.AdminId == null)
                {
                    ViewData["UserIsAdmin"] = "False";
                    // redirect if user isnt logged in
                    return Redirect("/Account/Login");
                }
            }
            else
            {
                ViewData["UserIsAdmin"] = "False";
                // redirect if user isnt logged in
                return Redirect("/Account/Login");
            }

            // create new object
            // reference https://davidsekar.com/c-sharp/modify-or-delete-entity-object-without-select

            GiftItems item = new GiftItems() { GiftItemsId = id };

            // attach to entity
            context.GiftItems.Attach(item);

            // remove the record
            context.GiftItems.Remove(item);

            // save the changes
            context.SaveChanges();

            return RedirectToAction("Index");
        }
    }
}