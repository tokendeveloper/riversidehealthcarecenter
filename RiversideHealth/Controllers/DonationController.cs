﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using System.Data.SqlClient;
using RiversideHealth.Models;
using RiversideHealth.Models.ViewModels;
using RiversideHealth.Data;
using Microsoft.AspNetCore.Http;
using System.Diagnostics;
using Stripe;

namespace RiversideHealth.Controllers
{
    public class DonationController : Controller
    {
        private readonly RiversideHealthDbContext context;

        public DonationController(RiversideHealthDbContext ctx)
        {
            context = ctx;
        }

        public ActionResult Index()
        {
            return View();
        }

        public ActionResult List()
        {
            IEnumerable<Donator> donatorList = context.Donators.ToList();

            return View(donatorList);
        }

        public ActionResult Summary()
        {
            return View();
        }

        [HttpPost]
        public ActionResult Donate(string stripeToken, string stripeEmail, string CustFName, string CustLName, string CustPhone, decimal DonationAmount, bool isAnon)
        {
            int donatorID = 0;
            string fullName = "";
            Donator donator = new Donator()
            {
                DonatorFirstName = CustFName,
                DonatorLastName = CustLName,
                DonatorEmail = stripeEmail,
                DonatorPhone = CustPhone,
                IsAnonymous = isAnon == true ? 1 : 0
            };

            // check if the donator exists by email, if it exists get info
        
            // check existing entity
            // reference https://stackoverflow.com/questions/1802286/best-way-to-check-if-object-exists-in-entity-framework/4689621
            var checkExistingCust = context.Donators.Any(d => d.DonatorEmail == stripeEmail);

            if (checkExistingCust)
            {   
                // searching for a record
                // reference https://docs.microsoft.com/en-us/ef/ef6/querying/
                Donator old_donator = context.Donators.Where(d => d.DonatorEmail == stripeEmail).FirstOrDefault();
                // copy the object
                donator = old_donator;
                // if donator already exists, pull the id and store it in the transaction
                donatorID = donator.DonatorId;
                Debug.WriteLine("Old Donator Found!");
                Debug.WriteLine(old_donator.DonatorFirstName);
                Debug.WriteLine(old_donator.DonatorLastName);
                Debug.WriteLine(old_donator.DonatorEmail);
                Debug.WriteLine(old_donator.DonatorPhone);
                Debug.WriteLine(old_donator.DonatorId);
                Debug.WriteLine(old_donator.DonatorRefID);
                Debug.WriteLine(donator);
                Debug.WriteLine(donator.DonatorFirstName);
                Debug.WriteLine(donator.DonatorLastName);
                Debug.WriteLine(donator.DonatorEmail);
                Debug.WriteLine(donator.DonatorPhone);
                Debug.WriteLine(donator.DonatorId);
                Debug.WriteLine(donator.DonatorRefID);
            }
            else
            {
                // create a new customer in stripe
                var custInfo = new CustomerCreateOptions
                {
                    SourceToken = stripeToken,
                    Description = donator.DonatorFirstName.ToString() + " " + donator.DonatorLastName.ToString(),
                    Email = stripeEmail,
                    Metadata = new Dictionary<string, string>()
                    {
                        {"First Name", donator.DonatorFirstName.ToString()},
                        {"Last Name", donator.DonatorLastName.ToString()},
                        {"Phone Number", donator.DonatorPhone.ToString()}
                    }
                };

                var new_customer = new CustomerService();
                Customer customer = new_customer.Create(custInfo);
                // store the customerID in the donator entity
                Debug.WriteLine(customer.Id);
                // store the generated customerID as a referenceID
                donator.DonatorRefID = customer.Id;
                donatorID = donator.DonatorId;
                Debug.WriteLine(donatorID);

                // store in database
                // store as a new donator if the email does not exist
                Debug.WriteLine(donator);
                context.Donators.Add(donator);
                context.SaveChanges();
            }

            fullName = donator.DonatorFirstName.ToString() + " " + donator.DonatorLastName.ToString();
            
            // create a charge object
            var donateInfo = new ChargeCreateOptions
            {
                Amount = (long)DonationAmount * 100,
                Currency = "cad",
                Description = "Donation from " + donator.DonatorFirstName.ToString() + " " + donator.DonatorLastName.ToString(),
                CustomerId = donator.DonatorRefID.ToString(),
                ReceiptEmail = donator.DonatorEmail.ToString() // send a receipt to the donator
            };

            var new_charge = new ChargeService();
            // store charge details
            Charge charge = new_charge.Create(donateInfo);
            // get the timestamp for the transaction date and convert to local time
            DateTime transDate = charge.Created.ToLocalTime();
            string transStatus = charge.Status.ToString();
            string referenceID = charge.Id;
            string receiptURL = charge.ReceiptUrl.ToString();
            Debug.WriteLine(transDate);
            Debug.WriteLine(transStatus);
            Debug.WriteLine(referenceID);
            // create a transaction if the payment was successful
            Transaction transaction = new Transaction()
            {
                TransactionAmount = DonationAmount,
                DonatorId = donatorID,
                TransactionStatus = transStatus,
                ReferenceId = referenceID,
                TransactionDate = transDate
            };

            // save changes
            context.Transactions.Add(transaction);
            context.SaveChanges();

            // create a Payment Summary model to return to the view
            DonationSummary summary = new DonationSummary()
            {
                CustID = donator.DonatorRefID,
                CustFullName = fullName,
                CustEmail = donator.DonatorEmail.ToString(),
                DonationReceiptLink = receiptURL,
                ReferenceID = referenceID,
                Status = transStatus,
                Amount = "$" + DonationAmount.ToString()
            };

            return View("Summary", summary);
        }

    }
}