﻿using System.Threading.Tasks;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using RiversideHealth.Models;
using RiversideHealth.Data;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using System.Diagnostics;

//References Christine's code.
//Correction. References A LOT of Christine's code. 

namespace RiversideHealth.Controllers
{
    public class AdminController : Controller
    {
        private readonly RiversideHealthDbContext db;
        private readonly IHostingEnvironment _env;
        private readonly UserManager<ApplicationUser> _userManager;
        private async Task<ApplicationUser> GetCurrentUserAsync() => await _userManager.GetUserAsync(HttpContext.User);

        public AdminController(RiversideHealthDbContext context,IHostingEnvironment env, UserManager<ApplicationUser> usermanager)
        {
            db = context;
            _env = env;
            _userManager = usermanager;
        }
        public async Task<ActionResult> Index()
        {
            var user = await GetCurrentUserAsync();
            if (user != null)
            {
                if (user.AdminId == null) { ViewData["UserHasAdmin"] = "False"; }
                else { ViewData["UserHasAdmin"] = user.AdminId.ToString(); }
                return View(await db.Admins.ToListAsync());
            }
            else
            {
                ViewData["UserHasAdmin"] = "None";
                return View(await db.Admins.ToListAsync());
            }
        }

        public ActionResult Show(int id)
        {
            return RedirectToAction("Details/"+id);
        }

        public async Task<ActionResult> Details(int? id)
        {
            var user = await GetCurrentUserAsync();
            if (id == null)
            {
                return new StatusCodeResult(400);
            }
            Admin located_admin = await db.Admins.SingleOrDefaultAsync(a=>a.AdminId==id);
            if (located_admin == null)
            {
                return NotFound();
            }

            if (user != null) { 
                if (user.Admin == located_admin)
                {
                    ViewData["UserIsAdmin"] = "True"; 
                }
                else
                {
                    ViewData["UserIsAdmin"] = "False";
                }
            }
            else
            {
                ViewData["UserIsAdmin"] = "False"; 
            }
            
            return View(located_admin);
        }

        public ActionResult Create()
        {
            return View();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Create([Bind("AdminId,FName,LName")] Admin Admin)
        {
            
            if (ModelState.IsValid)
            {
                db.Admins.Add(Admin);
                db.SaveChanges();
                var res = await MapUserToAdmin(Admin);
                
                return RedirectToAction("Index");
            }
            else
            {
                return RedirectToAction("Index");
            }
            
            
       
        }
        
        public async Task<ActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return new StatusCodeResult(400);
            }
            Admin Admin = db.Admins.Find(id);
            if (Admin == null)
            {
                //debug msg
                Debug.WriteLine("Not Found!");
                return NotFound();
            }
            var user = await GetCurrentUserAsync();
            if (user == null)
            {
                Debug.WriteLine("No user");
                return Forbid();
            }
            if (user.AdminId != id)
            {
                Debug.WriteLine("Wrong ID");
                return Forbid(); 
            }
            return View(Admin);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Edit([Bind("AdminId,FName,LName")] Admin Admin, int AdminId)
        {
            Debug.WriteLine("AdminId" + AdminId);


            var user = await GetCurrentUserAsync();

            Debug.WriteLine("User.AdminId" + user.AdminId);

            if (user == null) return Forbid();
            if (user.AdminId != AdminId)
            {
                Debug.WriteLine("Wrong Admin!");
                return Forbid();
            }

            if (ModelState.IsValid)
            {
                db.Entry(Admin).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(Admin);
        }

        public async Task<ActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return new StatusCodeResult(400);
            }

            Admin Admin = await db.Admins.FindAsync(id);
            if (Admin == null)
            {
                return NotFound();
            }
            var user = await GetCurrentUserAsync();
            if (user == null) return Forbid();
            if (user.AdminId != id)
            {
                return Forbid();
            }
            return View(Admin);
        }

        //Is executed upon Delete action.
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> DeleteConfirmed(int id)
        {
            Admin Admin = await db.Admins.FindAsync(id);
            var user = await GetCurrentUserAsync();
            if (user.AdminId != id)
            {
                return Forbid();
            }
            await UnmapUserFromAdmin(id);
            db.Admins.Remove(Admin);
            await db.SaveChangesAsync();
            return RedirectToAction("Index");
        }

        public async Task<IActionResult> UnmapUserFromAdmin(int id)
        {
            Admin Admin = await db.Admins.FindAsync(id);
            Admin.User = null;
            Admin.UserId = "";
            if (ModelState.IsValid)
            {
                db.Entry(Admin).State = EntityState.Modified;
                var admin_res = await db.SaveChangesAsync();
                if (admin_res == 0)
                {
                    return BadRequest(admin_res);
                }
                else
                {
                    var user = await GetCurrentUserAsync();
                    user.Admin = null;
                    user.AdminId = null;
                    var user_res = await _userManager.UpdateAsync(user);
                    if (user_res == IdentityResult.Success)
                    {
                        Debug.WriteLine("User Updated");
                        return Ok();
                    }
                    else
                    {
                        Debug.WriteLine("User not updated");
                        return BadRequest(user_res);
                    }
                }
            }
            else
            {
                return BadRequest("Unstable Model.");
            }
            
        }

        private async Task<IActionResult> MapUserToAdmin(Admin Admin)
        {
            var user = await GetCurrentUserAsync();
            user.Admin = Admin;
            var user_res = await _userManager.UpdateAsync(user);
            if (user_res == IdentityResult.Success)
            {
                Debug.WriteLine("Mapped the admin to user.");
            }
            else
            {
                Debug.WriteLine("Unable to map the admin to the user.");
                return BadRequest(user_res);
            }
            Admin.User = user;
            Admin.UserId = user.Id;
            if (ModelState.IsValid)
            {
                db.Entry(Admin).State = EntityState.Modified;
                var admin_res = await db.SaveChangesAsync();
                if (admin_res > 0)
                {
                    return Ok();
                }
                else
                {
                    return BadRequest(admin_res);
                }
            }
            else
            {
                return BadRequest("Unstable Admin Model.");
            }

        }        
        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
        
    }
}