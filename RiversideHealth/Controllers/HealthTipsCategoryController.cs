﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using System.Data.SqlClient;
using RiversideHealth.Models;
using RiversideHealth.Data;
using Microsoft.AspNetCore.Http;
using System.Diagnostics;

namespace RiversideHealth.Controllers
{
    public class HealthTipsCategoryController : Controller
    {
        private readonly RiversideHealthDbContext context;
        private readonly UserManager<ApplicationUser> _userManager;
        private async Task<ApplicationUser> GetCurrentUserAsync() => await _userManager.GetUserAsync(HttpContext.User);

        public HealthTipsCategoryController(RiversideHealthDbContext ctx, UserManager<ApplicationUser> userManager)
        {
            context = ctx;
            _userManager = userManager;
        }

        public async Task<ActionResult> Index(int page)
        {
            var admin = await GetCurrentUserAsync();

            if (admin != null)
            {
                if (admin.AdminId != null)
                {
                    ViewData["AdminId"] = admin.AdminId.ToString();
                }
                else
                {
                    //return Redirect("/Account/Login");
                    ViewData["AdminId"] = "None";
                }
            }
            else
            {
                //return Redirect("/Account/Login");
                ViewData["AdminId"] = "None";
            }


            // automatically create an uncategorized category for deletion purposes
            if(context.HealthTipsCategories.Any(hc => hc.CategoryTitle == "Uncategorized"))
            {
                Debug.WriteLine("It Exists");
            }
            else
            {
                HealthTipsCategory uncategorized = new HealthTipsCategory()
                {
                    CategoryTitle = "Uncategorized"
                };

                // insert query
                context.HealthTipsCategories.Add(uncategorized);
                context.SaveChanges();
            }


            /*Pagination Algorithm*/
            // I based this on christine's code
            var myCategories = await context.HealthTipsCategories.ToListAsync();
            int pagecount = myCategories.Count();
            int pageItems = 8;
            int limit = (int)Math.Ceiling((decimal)pagecount / pageItems) - 1;

            limit = limit < 0 ? 0 : limit;
            page = page < 0 ? 0 : page;
            page = page > limit ? limit : page;

            int initial = pageItems * page;

            ViewData["page"] = (int)page;
            ViewData["PageSummary"] = "";

            if (limit > 0)
            {
                ViewData["PageSummary"] =
                    (page + 1).ToString() + " of " +
                    (limit + 1).ToString();
            }

            List<HealthTipsCategory> categories = await context.HealthTipsCategories.ToListAsync();

            return View(categories);
        }

        public ActionResult Details(int id)
        {
            return View(context.HealthTipsCategories.Find(id));
        }

        public async Task<ActionResult> Create()
        {
            var admin = await GetCurrentUserAsync();
            if (admin != null)
            {
                if (admin.AdminId == null)
                {
                    ViewData["UserIsAdmin"] = "False";
                    // redirect if user isnt logged in
                    return Redirect("/Account/Login");
                }
                else
                {
                    ViewData["UserIsAdmin"] = admin.AdminId.ToString();
                }
            }
            else
            {
                ViewData["UserIsAdmin"] = "False";
                // redirect if user isnt logged in
                return Redirect("/Account/Login");
            }
            
            return View();
        }

        public ActionResult Edit(int id)
        {
            return View(context.HealthTipsCategories.Find(id));
        }

        [HttpPost]
        public ActionResult Create(string CategoryTitle_new)
        {
            HealthTipsCategory htc = new HealthTipsCategory()
            {
                CategoryTitle = CategoryTitle_new
            };

            // insert query
            context.HealthTipsCategories.Add(htc);

            context.SaveChanges();

            return Redirect("Index");
        }

        [HttpPost]
        public ActionResult Update(int id, string CategoryTitle_update)
        {
            // create a new entity object
            // reference https://davidsekar.com/c-sharp/modify-or-delete-entity-object-without-select
            HealthTipsCategory htc = new HealthTipsCategory()
            {
                TipsCategoryId = id
            };

            // attach the entity
            context.HealthTipsCategories.Attach(htc);

            // add the update
            htc.CategoryTitle = CategoryTitle_update;

            // update the change
            context.SaveChanges();

            return Redirect("/HealthTipsCategory/" + id);
        }

        // I added async to make the delete function secure
        public async Task<ActionResult> Delete(int id)
        {
            // create new entity object
            // reference https://davidsekar.com/c-sharp/modify-or-delete-entity-object-without-select

            var admin = await GetCurrentUserAsync();
            if (admin != null)
            {
                if (admin.AdminId == null)
                {
                    ViewData["UserIsAdmin"] = "False";
                    // redirect if user isnt logged in
                    return Redirect("/Account/Login");
                }
                else
                {
                    ViewData["UserIsAdmin"] = admin.AdminId.ToString();
                }
            }
            else
            {
                ViewData["UserIsAdmin"] = "False";
                // redirect if user isnt logged in
                return Redirect("/Account/Login");
            }

            // check uncategorized category
            int catId = 0;

            if (context.HealthTipsCategories.Any(hc => hc.CategoryTitle == "Uncategorized"))
            {
                Debug.WriteLine("it exists");
                // remove all dependent entries first

                HealthTipsCategory uncat = context.HealthTipsCategories.Where(ct => ct.CategoryTitle == "Uncategorized").FirstOrDefault();
                catId = uncat.TipsCategoryId;

                Debug.WriteLine("new ID: " + catId);

                // we can set the category  to uncategorized
                string query = "UPDATE HealthTips SET TipsCategoryId = @uncat WHERE TipsCategoryId = @id";
                SqlParameter[] catSettings = new SqlParameter[2];

                catSettings[0] = new SqlParameter("@uncat", catId);
                catSettings[1] = new SqlParameter("@id", id);

                context.Database.ExecuteSqlCommand(query, catSettings);
            }

            HealthTipsCategory htc = new HealthTipsCategory() { TipsCategoryId = id };

            // attach to entity
            context.HealthTipsCategories.Attach(htc);

            // remove the record
            context.HealthTipsCategories.Remove(htc);

            // save the changes
            context.SaveChanges();

            return RedirectToAction("Index");
        }
    }
}